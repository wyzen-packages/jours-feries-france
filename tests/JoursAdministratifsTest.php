<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Wyzen\Php\JoursAdministratifs;

final class JoursAdministratifsTest extends TestCase
{
    /**
     * testIsJourOuvre
     *
     * @testdox Est-ce un jour ouvré ?
     */
    public function testIsJourOuvre(): void
    {
        $checks = [
            '25-12-2017' => false,
            '26-12-2017' => true, //lundi
            '31-10-2021' => false, // dimanche
            '30-10-2021' => false, // samedi
            '29-10-2021' => true, //vendredi
        ];

        foreach ($checks as $d => $v) {
            $this->assertEquals(
                $v,
                JoursAdministratifs::isJourOuvre(new DateTime($d)),
                ($v === true ? "{$d} n'est pas un jour ouvré." : "{$d} est bien un jour ouvré.")
            );
        }

        // on test un jour férié spécifique d'une zone
        $d = "22-05-2020";// Abolition de l'esclavage, férié en Martinique mais pas en Métropole
        $this->assertEquals(false, JoursAdministratifs::isJourOuvre(new DateTime($d), 'Martinique'));
    }


    /**
     * testIsJourOuvre
     *
     * @testdox Est-ce un jour ouvrable ?
     */
    public function testIsJourOuvrable(): void
    {
        $checks = [
            '25-12-2017' => false,
            '26-12-2017' => true, // lundi
            '31-10-2021' => false, // dimanche
            '30-10-2021' => true, // samedi
            '29-10-2021' => true, //vendredi
        ];

        foreach ($checks as $d => $v) {
            $this->assertEquals(
                $v,
                JoursAdministratifs::isJourOuvrable(new DateTime($d)),
                ($v === true ? "{$d} n'est pas un jour ouvrable." : "{$d} est bien un jour ouvrable.")
            );
        }

        // on test un jour férié spécifique d'une zone
        $d = "22-05-2020";// Abolition de l'esclavage, férié en Martinique mais pas en Métropole
        $this->assertEquals(
            false,
            JoursAdministratifs::isJourOuvrable(new DateTime($d), 'Martinique')
        );
    }


    /**
     * testAddJourCalendaire.
     *
     * @testdox Adding JourCalendaire days should return new datetime `x` days in the future
     */
    public function testAddJourCalendaire(): void
    {
        $this->assertEquals(
            new DateTime('30-12-2017'),
            JoursAdministratifs::addJourCalendaire(new DateTime('20-12-2017'), 10)
        );
    }

    /**
     * testSubJourCalendaire.
     *
     * @testdox Subtracting JourCalendaire days should return new datetime `x` days in the past
     */
    public function testSubJourCalendaire(): void
    {
        $this->assertEquals(
            new DateTime('20-12-2017'),
            JoursAdministratifs::subJourCalendaire(new DateTime('30-12-2017'), 10)
        );
    }

    /**
     * testAddJourOuvrable.
     *
     * @testdox Adding JourOuvrable days should return new datetime `x` days in the future skipping sundays and férié
     */
    public function testAddJourOuvrable(): void
    {
        // $d = new DateTimeImmutable('03-01-2018');
        $d         = new DateTime('03-01-2018');
        $dRequired = new DateTime('15-01-2018');

        $newdate = JoursAdministratifs::addJourOuvrable($d, 10);

        $this->assertEquals(new DateTime('03-01-2018'), $d);
        $this->assertEquals($dRequired, $newdate);
    }

    /**
     * testAddJourOuvrable in Alsace-Moselle.
     *
     * @testdox Adding JourOuvrable days in Alsace-Moselle during Noël should return 1 day later than Métropole
     */
    public function testAddJourOuvrableAM(): void
    {
        // $d = new DateTimeImmutable('20-12-2017');
        $d         = new DateTime('20-12-2017');
        $dRequired = new DateTime('04-01-2018');

        $newdate = JoursAdministratifs::addJourOuvrable($d, 10, 'Alsace-Moselle');

        $this->assertEquals(new DateTime('20-12-2017'), $d);
        $this->assertEquals($dRequired, $newdate);
    }

    /**
     * testSubJourOuvrable.
     *
     * @testdox Subtracting JourOuvrable days should return new datetime `x` days in the past skipping sundays and férié
     */
    public function testSubJourOuvrable(): void
    {
        $this->assertEquals(
            new DateTime('18-12-2017'),
            JoursAdministratifs::subJourOuvrable(new DateTime('30-12-2017'), 10)
        );
    }

    /**
     * testAddJourOuvre.
     *
     * @testdox Adding JourOuvre days should return new datetime `x` days in the future skipping saturday, sundays and férié
     */
    public function testAddJourOuvre(): void
    {
        $this->assertEquals(
            new DateTime('05-01-2018'),
            JoursAdministratifs::addJourOuvre(new DateTime('20-12-2017'), 10)
        );
    }

    /**
     * testAddJourOuvre in Alsace-Moselle.
     *
     * @testdox Adding JourOuvre days in Alsace-Moselle during Noël should return 3 day later than Métropole (because there's a week-end)
     */
    public function testAddJourOuvreAM(): void
    {
        $this->assertEquals(
            new DateTime('08-01-2018'),
            JoursAdministratifs::addJourOuvre(new DateTime('20-12-2017'), 10, 'Alsace-Moselle')
        );
    }

    /**
     * testSubJourOuvre.
     *
     * @testdox Subtracting JourOuvre days should return new datetime `x` days in the past skipping saturday, sundays and férié
     */
    public function testSubJourOuvre(): void
    {
        $this->assertEquals(
            new DateTime('15-12-2017'),
            JoursAdministratifs::subJourOuvre(new DateTime('30-12-2017'), 10)
        );
    }

    /**
     * testAddJourFranc.
     *
     * @testdox Adding JourFranc days should return calendar days + skipping days 6 ans 7 and férié for last day
     */
    public function testAddJourFranc(): void
    {
        $this->assertEquals(
            new DateTime('28-12-2020'),
            JoursAdministratifs::addJourFranc(new DateTime('21-12-2020'), 6)
        );
        $this->assertEquals(
            new DateTime('28-12-2020'),
            JoursAdministratifs::addJourFranc(new DateTime('21-12-2020'), 3)
        );
        // exemples pris ici :
        // https://www.service-public.fr/particuliers/vosdroits/F31111
        $this->assertEquals(
            new DateTime('21-12-2020'),
            JoursAdministratifs::addJourFranc(new DateTime('09-12-2020'), 10)
        );
        $this->assertEquals(
            new DateTime('11-12-2020'),
            JoursAdministratifs::addJourFranc(new DateTime('30-11-2020'), 10)
        );
        $this->assertEquals(
            new DateTime('28-12-2020'),
            JoursAdministratifs::addJourFranc(new DateTime('14-12-2020'), 10)
        );
        $this->assertEquals(
            new DateTime('25-10-2021'),
            JoursAdministratifs::addJourFranc(new DateTime('23-09-2021'), 30)
        );
    }

    /**
     * testAddJourFrancAM.
     *
     * @testdox Adding JourFranc days in Alsace Moselle should return 1 day later (because there is 1 more férié day after chirstmas, les chanceux)
     */
    public function testAddJourFrancAM(): void
    {
        // J'ai pris une année où Noël et le 2e jour de Noël ne tombent pas un samedi :)
        $this->assertEquals(
            new DateTime('28-12-2016'),
            JoursAdministratifs::addJourFranc(new DateTime('21-12-2016'), 6, 'Alsace-Moselle')
        );
        $this->assertEquals(
            new DateTime('27-12-2016'),
            JoursAdministratifs::addJourFranc(new DateTime('21-12-2016'), 3, 'Alsace-Moselle')
        );
    }


    /**
     * testAddMoisFranc.
     *
     * @testdox Ajouter X mois francs à une date.
     */
    public function testAddMoisFranc(): void
    {
        $this->assertEquals(
            new DateTime('22-03-2021'),
            JoursAdministratifs::addMoisFranc(new DateTime('19-12-2020'), 3)
        );
        $this->assertEquals(
            new DateTime('23-03-2021'),
            JoursAdministratifs::addMoisFranc(new DateTime('22-12-2020'), 3)
        );
        $this->assertEquals(
            new DateTime('28-02-2019'), // Le 31/02 (30+1) n'existant pas, on tombe au 28/02
            JoursAdministratifs::addMoisFranc(new DateTime('30-01-2019'), 1)
        );

        $this->assertEquals(
            new DateTime('01-03-2021'), // en 2021, le 28/02 est un dimanche donc on passe au 1er Mars
            JoursAdministratifs::addMoisFranc(new DateTime('30-01-2021'), 1)
        );

        $this->assertEquals(
            new DateTime('01-03-2019'),
            // à cause du 'dies a quo' on commence à compter le saut du/des mois à partir du 31/01+1j soit le 01/02
            // c'est donc normald'obtenir le 01/03
            JoursAdministratifs::addMoisFranc(new DateTime('31-01-2019'), 1)
        );
    }


    public function testDiffJour(): void
    {
        $tests = [
            ['19-12-2020', '31-12-2020', 'ouvré', 7],
            ['19-12-2020', '31-12-2020', 'ouvrable', 9],
            ['01-05-2020', '31-05-2020', 'ouvré', 18]
        ];

        foreach ($tests as $t) {
            $this->assertEquals(
                $t[3], // résultat attendu (en nb de jours)
                JoursAdministratifs::diffJour(
                    new DateTime($t[0]),
                    new DateTime($t[1]),
                    'jour_' . $t[2], // jour_ouvré ou jour_calendaire
                ),
                "Non. il n'y a pas {$t[3]} jours {$t[2]} entre le {$t[0]} et le {$t[1]}."
            );
        }

        // Cas special de test d'une zone
        $this->assertEquals(
            17, //  17 jours ouvré en Martinique en Mai 2020
            JoursAdministratifs::diffJour(
                new DateTime('01-05-2020'),
                new DateTime('31-05-2020'),
                'jour_ouvré',
                'Martinique' // oùle 22/05 est ferié
            )
        );
    }
}
