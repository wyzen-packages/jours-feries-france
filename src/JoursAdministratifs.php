<?php

/**
 * Calcul des jours Ouvrables/Ouvrés
 */

declare(strict_types=1);

namespace Wyzen\Php;

use DateInterval;
use DateTime;
use DateTimeInterface;
use Exception;

/**
 * Calcul des jours Ouvrables/Ouvrés
 */
class JoursAdministratifs
{
    /**
     * Increments $date by $days days and return the result.
     *
     * @param  DateTime $date         DateTime to increment
     * @param  int      $days         number of days to add (or sub if negative)
     * @param  array    $rejectDays   array of days of the week (monday =1, sunday =7) that skips incrementing date
     * @param  bool     $rejectFeries if current day is ferié, skip incremeting
     * @param  string   $zone         geographic zone for férié days (see JoursFeries::ZONES)
     *
     * @return DateTime
     */
    private static function walkJours(DateTime $date, int $days, array $rejectDays = [], bool $rejectFeries = true, string $zone = 'Métropole'): DateTime
    {
        $inc         = 0;
        $dayInterval = DateInterval::createFromDateString('1 day');
        while ($inc < abs($days)) {
            if ($days > 0) {
                $date->add($dayInterval);
            } else {
                $date->sub($dayInterval);
            }
            if ((!$rejectFeries || !JoursFeries::isFerie($date, $zone))
                && !in_array($date->format('N'), $rejectDays)
            ) {
                $inc++;
            }
        }

        return $date;
    }

    /**
     * @param DateTimeInterface $d    La date a tester.
     *
     * @param string            $zone geographic zone for férié days (see
     *                                JoursFeries::ZONES)
     *
     *
     * @return bool     Renvoie VRAI si $d n'est pas un samedi, dimanche ou un jour férié
     */
    public static function isJourOuvre(DateTimeInterface $d, $zone = 'Métropole'): bool
    {
        return false === (in_array($d->format('N'), ['6', '7']) || JoursFeries::isFerie($d, $zone)
        );
    }

    /**
     * @param DateTimeInterface $d    La date a tester.
     *
     * @param string            $zone geographic zone for férié days (see
     *                                JoursFeries::ZONES)
     *
     *
     * @return boolean      Renvoie VRAI si $d n'est pas un dimanche ou jour férié
     */
    public static function isJourOuvrable(DateTimeInterface $d, $zone = 'Métropole'): bool
    {
        return false === ($d->format('N') === '7' || JoursFeries::isFerie($d, $zone)
        );
    }

    /**
     * Returns DateTime set up from $date with $days in the future .
     *
     * @param  DateTime $date starting date
     * @param  int      $days days to add
     *
     * @return DateTime
     */
    public static function addJourCalendaire(DateTime $date, int $days): DateTime
    {
        $d = clone $date;
        return $d->add(DateInterval::createFromDateString($days . ' day'));
    }

    /**
     * Returns DateTime set up from $date with $days in the past .
     *
     * @param  DateTime $date starting date
     * @param  int      $days days to sub
     *
     * @return DateTime
     */
    public static function subJourCalendaire(DateTime $date, int $days): DateTime
    {
        $d = clone $date;
        return self::addJourCalendaire($d, -1 * abs($days));
    }

    /**
     * Returns DateTime set up from $date with $days in the future not counting sundays and fériés.
     *
     * @param  DateTime $date starting date
     * @param  int      $days days to add
     * @param  string   $zone geographic zone for férié days (see
     *                        JoursFeries::ZONES)
     *
     * @return DateTime
     */
    public static function addJourOuvrable(DateTime $date, int $days, string $zone = 'Métropole'): DateTime
    {
        $d = clone $date;
        return self::walkJours($d, $days, [7], true, $zone);
    }

    /**
     * Returns DateTime set up from $date with $days in the past not counting sundays and fériés.
     *
     * @param  DateTime $date starting date
     * @param  int      $days days to sub
     * @param  string   $zone geographic zone for férié days (see
     *                        JoursFeries::ZONES)
     *
     * @return DateTime
     */
    public static function subJourOuvrable(DateTime $date, int $days, string $zone = 'Métropole'): DateTime
    {
        $d = clone $date;
        return self::walkJours($d, -1 * abs($days), [7], true, $zone);
    }

    /**
     * Returns DateTime set up from $date with $days in the future not counting saturdays, sundays and fériés.
     *
     * @param  DateTime $date starting date
     * @param  int      $days days to add
     * @param  string   $zone geographic zone for férié days (see
     *                        JoursFeries::ZONES)
     *
     * @return DateTime
     */
    public static function addJourOuvre(DateTime $date, int $days, string $zone = 'Métropole'): DateTime
    {
        $d = clone $date;
        return self::walkJours($d, $days, [6, 7], true, $zone);
    }

    /**
     * Returns DateTime set up from $date with $days in the past not counting saturdays, sundays and fériés.
     *
     * @param  DateTime $date starting date
     * @param  int      $days days to sub
     * @param  string   $zone geographic zone for férié days (see
     *                        JoursFeries::ZONES)
     *
     * @return DateTime
     */
    public static function subJourOuvre(DateTime $date, int $days, string $zone = 'Métropole'): DateTime
    {
        $d = clone $date;
        return self::walkJours($d, -1 * abs($days), [6, 7], true, $zone);
    }

    /**
     * Ajout de jours-francs.
     * Un délai franc en jours se compte de quantième à quantième, le jour de départ
     * ne comptant pas et le jour de l'échéance non plus. Autrement dit, il court à compter
     * du lendemain du jour de départ et comprend la journée qui suit le jour où il prend fin.
     * Les jours sont calendaires, mais s'il se termine sur un jour non-ouvrés, alors on décale
     * jusqu'au prochain jour ouvré.
     * Ex: Départ Lundi + 2 jours francs = Jeudi
     * Ex: Départ jeudi + 2jours francs = mardi suivant
     *
     * Exemple :
     * Ressources :
     * - https://www.attestis.com/calculateur-de-delai-franc/
     * - https://www.demarches.interieur.gouv.fr/particuliers/jour-ouvrable-jour-ouvre-jour-franc-jour-calendaire-quelles-differences.
     * -
     *
     * @param  DateTime $date Date de départ
     * @param  int      $days Nombre de jours à
     *                        ajouter
     * @param  string   $zone geographic Pour le calcul des jours fériés (see
     *                        JoursFeries::ZONES)
     *
     * @return DateTime
     */
    public static function addJourFranc(DateTime $date, int $days, string $zone = 'Métropole'): DateTime
    {
        $d = clone $date;
        if ($days < 0) {
            throw new Exception("The number of days shouldn't be negative for jours francs, the results wouldn't be accurate.", 1);
        }
        $d->add(DateInterval::createFromDateString($days . ' day'));

        return self::walkJours($d, 1, [6, 7], true, $zone);
    }

    /*
     * Ajout de mois franc.
     * Similaire à l'ajout de jour franc, mais en mois.
     *
     * Le délai franc en mois se compte si possible de date à date : il s'achève théoriquement le même
     * jour que celui du départ mais d'un autre mois (le dernier du délai). Si le jour ainsi calculé de
     * fin du délai n'existe pas (30 février, 31 juin, etc.), il est réduit au dernier jour réel du
     * mois concerné.
     *
     * Lorsque le dernier jour du délai franc tombe un samedi, un dimanche ou un jour férié, la date limite
     * est reportée au premier jour ouvrable suivant.
     *
     *
     * @param DateTime $date Date de départ
     * @param int $days Nombre de mois francs à ajouter
     * @param string $zone geographic Pour le calcul des jours fériés (see JoursFeries::ZONES)
     *
     * @return DateTime
     */
    public static function addMoisFranc(DateTime $date, int $months, string $zone = 'Métropole'): DateTime
    {
        $d = clone $date;
        if ($months < 0) {
            throw new Exception("The number of months shouldn't be negative for mois francs, the results wouldn't be accurate.", 1);
        }

        // Saute la date de début (dies a quo)
        $d->add(DateInterval::createFromDateString('1 day')); //

        // On veut avancer d'un mois mais …
        // On ne peut PAS faire `un +x month` via DateInterval
        // (ex: 31/01 + 1 month = 3 mars, alors qu'on veut le 28 fev ici)
        // du coup, on va calculer le saut des mois au 15th du mois
        /**
         * @var DateTime $fifteenth
         */
        $fifteenth = clone $d;
        $fifteenth->setDate(\intval($fifteenth->format('Y')), \intval($fifteenth->format('m')), 15);

        // on avance les mois pour trouver la date de destination (mais au 15th pour éviter les problèmes)
        $fifteenth->add(DateInterval::createFromDateString($months . ' month'));

        // maintenant qu'on a la bonne date de destination, mais au 15th, on va vérifier si pour le jour
        // prévu originellement cette date existe bien (et qu'on tombe pas sur un 31 février par exemple)
        if (checkdate(\intval($fifteenth->format('m')), \intval($d->format('d')), \intval($fifteenth->format('Y')))) {
            // La date existe, donc on va la créer
            // (année et mois d'après le $fifteenth, et le jour de la date originelle)
            $destinationDate = new DateTime(
                $fifteenth->format('Y') . '-' . $fifteenth->format('m') . '-' . $d->format('d')
            );
        } else {
            // La date de destination théorique n'existe pas, on va donc juste prendre le dernier jour
            // du mois théorique (du mois de $fifteenth)
            $destinationDate = clone $fifteenth;
            $destinationDate->setDate(
                // Grâce à format("t") on obtient le dernier jour du mois de la date
                \intval($destinationDate->format("Y")),
                \intval($destinationDate->format("m")),
                \intval($destinationDate->format("t"))
            );
        }

        // Ici, on a déjà compté le 'dies a quo' au début (avant d'avancer le mois) donc on ne
        // va devoir "walker" que si le jour de destination est déjà un férié ou sam/dim;
        if (JoursFeries::isFerie($destinationDate, $zone) || in_array($destinationDate->format('N'), [6, 7])) {
            return self::walkJours($destinationDate, 1, [6, 7], true, $zone);
        } else {
            return $destinationDate;
        }
    }

    public static function diffJour(DateTime $date1, DateTime $date2, $format = 'jour_calendaire', string $zone = 'Métropole'): ?int
    {
        $d1 = clone $date1;
        $d2 = clone $date2;
        if ($format === 'jour_calendaire') {
            $diff = date_diff($d1, $d2);
            if (!$diff) {
                return null;
            }
            return $diff->format('%a'); // "%a" renvoit le nbr TOTAL de jours
        }

        if ($d1 == $d2) {
            return 0;
        }
        $dateDebut = ($d1 < $d2 ? $d1 : $d2);
        $dateFin   = ($d1 > $d2 ? $d1 : $d2);
        $oneDay    = DateInterval::createFromDateString('1 day');
        $c         = $dateDebut;
        $count     = 0;
        while ($c != $dateFin) { // walk +1 day
            if ($format === 'jour_ouvré') {
                if (static::isJourOuvre($c, $zone)) {
                    $count++;
                }
            } elseif ($format === 'jour_ouvrable') {
                if (static::isJourOuvrable($c, $zone)) {
                    $count++;
                }
            } else {
                throw new Exception("Format inattendu. La différence en jour entre deux dates peut se faire en jour calendaire, en jour ouvré, ou en jour ouvrable mais `{$format}` n'est pas reconnu.", 1);
            }

            $c = $c->add($oneDay);
        }

        return $count;
    }
}
